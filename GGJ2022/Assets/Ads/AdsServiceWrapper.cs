using GameManager.Scripts;
using UnityEngine;

namespace Ads
{
    public class AdsServiceWrapper : ScriptableObject
    {
        public void ShowRewardedVideo()
        {
            var task = Services.Get<AdsService>().ShowRewardedVideo();
        }
        
        public void ShowInterstitialVideo()
        {
            var task = Services.Get<AdsService>().ShowInterstitialVideo();
        }
    }
}