using System.Threading;
using System.Threading.Tasks;
using GameManager.Scripts;
using UnityEngine;
using UnityEngine.Advertisements;

namespace Ads
{
    public class AdsService : ScriptableObject, IOpenAppService
    {
        [SerializeField] private string androidGameId;
        [SerializeField] private string iOSGameId;
        [SerializeField] private bool testMode = true;

        [Header("RewardedVideo")] 
        [SerializeField] private bool initRewardedVideoOnStart;
        [SerializeField] private string androidRewardedAdUnitId = "Rewarded_Android";
        [SerializeField] private string iOSRewardedAdUnitId = "Rewarded_iOS";
        
        [Header("InterstitialVideo")] 
        [SerializeField] private bool initInterstitialVideoOnStart = true;
        [SerializeField] private string androidInterstitialAdUnitId = "Interstitial_Android";
        [SerializeField] private string iOsInterstitialAdUnitId = "Interstitial_iOS";

        private AdsInitializer _initializeAds;
        private AdsManager _rewardedAdsService;
        private AdsManager _interstitialAdsService;
        private Task<bool> _initTask;

        public Task OpenApp(CancellationToken token = default)
        {
            _initializeAds = new AdsInitializer(androidGameId, iOSGameId, testMode);
            _rewardedAdsService = new AdsManager(androidRewardedAdUnitId, iOSRewardedAdUnitId);
            _interstitialAdsService = new AdsManager(androidInterstitialAdUnitId, iOsInterstitialAdUnitId);

            InitializeAds();
            return Task.CompletedTask;
        }

        private async void InitializeAds()
        {
            _initTask = _initializeAds.InitializeAds();
            if (!await _initTask) 
                return;
            if (initRewardedVideoOnStart)
                _rewardedAdsService.LoadAd();
            if (initInterstitialVideoOnStart)
                _interstitialAdsService.LoadAd();
        }

        public async Task<bool> ShowRewardedVideo()
        {
            if(!await _initTask)
                await _initializeAds.InitializeAds();
            return await _rewardedAdsService.ShowAd();
        }
        
        public async Task<bool> ShowInterstitialVideo()
        {
            if(!await _initTask)
                await _initializeAds.InitializeAds();
            return await _interstitialAdsService.ShowAd();
        }
    }

    public class AdsInitializer : IUnityAdsInitializationListener
    {
        private TaskCompletionSource<bool> _initTask;
        private readonly string _androidGameId;
        private readonly string _iOSGameId;
        private readonly bool _testMode;
        
        public AdsInitializer(string androidGameId, string iOSGameId, bool testMode)
        {
            _androidGameId = androidGameId;
            _iOSGameId = iOSGameId;
            _testMode = testMode;
        }
        
        public Task<bool> InitializeAds()
        {
#if UNITY_IOS
            Advertisement.Initialize(_iOSGameId, _testMode, this);
#elif UNITY_ANDROID
            Advertisement.Initialize(_androidGameId, _testMode, this);
#endif
            _initTask = new TaskCompletionSource<bool>();
            return _initTask.Task;
        }
        
        public void OnInitializationComplete()
        {
            _initTask.SetResult(true);
        }
 
        public void OnInitializationFailed(UnityAdsInitializationError error, string message)
        {
            Debug.LogError($"Initialization Unity Ads Failed with error {error.ToString()}: {message}");
            _initTask.SetResult(false);
        }
    }
    
    public class AdsManager : IUnityAdsLoadListener, IUnityAdsShowListener
    {
        private TaskCompletionSource<bool> _showAdTask;
        private TaskCompletionSource<bool> _loadAdTask;
        private readonly string _adUnitId; // This will remain null for unsupported platforms
 
        public AdsManager(string androidAdUnitId, string iOSAdUnitId)
        {
#if UNITY_IOS
            _adUnitId = iOSAdUnitId;
#elif UNITY_ANDROID
            _adUnitId = androidAdUnitId;
#endif
        }
 
        public void LoadAd()
        {
            // IMPORTANT! Only load content AFTER initialization.
            _loadAdTask = new TaskCompletionSource<bool>();
            Advertisement.Load(_adUnitId, this);
        }

        public void OnUnityAdsAdLoaded(string adUnitId)
        {
            if (adUnitId.Equals(_adUnitId))
                _loadAdTask.SetResult(true);
            else
                Debug.LogError($"Unity Ads Ad Loaded Wrong adUnitId: {adUnitId}");
        }
     
        public async Task<bool> ShowAd()
        {
            if (_loadAdTask == null)
                LoadAd();
            
            if (await _loadAdTask.Task)
            {
                _showAdTask = new TaskCompletionSource<bool>();
                Advertisement.Show(_adUnitId, this);
                return await _showAdTask.Task;
            }

            LoadAd();
            _showAdTask?.SetResult(false);
            return false;
        }
     
        public void OnUnityAdsShowComplete(string adUnitId, UnityAdsShowCompletionState showCompletionState)
        {
            if (adUnitId.Equals(_adUnitId))
            {
                _showAdTask.SetResult(showCompletionState.Equals(UnityAdsShowCompletionState.COMPLETED));
                LoadAd();
            }
            else
            {
                Debug.LogError($"Unity Ads Show Complete Wrong adUnitId: {adUnitId}");
            }
        }
     
        public void OnUnityAdsFailedToLoad(string adUnitId, UnityAdsLoadError error, string message)
        {
            Debug.LogError($"Unity Ads Failed To Load adUnitId: {adUnitId}");
            _loadAdTask.SetResult(false);
        }
     
        public void OnUnityAdsShowFailure(string adUnitId, UnityAdsShowError error, string message)
        {
            Debug.LogError($"Unity Ads Failed To Show adUnitId: {adUnitId}");
            _showAdTask.SetResult(false);
        }
     
        public void OnUnityAdsShowStart(string adUnitId) { }
        public void OnUnityAdsShowClick(string adUnitId) { }
    }
}