using System.Threading;
using System.Threading.Tasks;
using UnityEngine;

namespace GameManager.Scripts
{
    public class GameController : MonoBehaviour
    {  
        [SerializeField] private GameObject startingObj;
        [SerializeField] private ServiceManager serviceManager;     
        private CancellationTokenSource _cts; 

        private async void Start()
        {
            _cts = new CancellationTokenSource();
            startingObj.SetActive(false);
            
            try
            {
                await OpenApp(_cts.Token);
                
                Services.Get<Navigator>().Open(SceneName.HomePage);
            }
            catch (TaskCanceledException)
            {
                Debug.LogError("Application closed on OpenApp");
            }
            finally
            {
                _cts.Dispose();
            }
            _cts = null;
        }

        private async Task OpenApp(CancellationToken token = default)
        {
            await serviceManager.Init(token);
        } 
        
        private void OnDestroy()
        {
            _cts?.Cancel();
        }
    }
}
