using System.Collections;
using System.Threading;
using System.Threading.Tasks;
using Unity.Notifications.iOS;
using UnityEngine;

namespace GameManager.Scripts
{
    public class AuthorizationManager : ScriptableObject, IOpenAppService
    {
        public Task OpenApp(CancellationToken token = default)
        {
            IDeviceAuthorizationManager deviceAuthorizationManager = null;
            if (Application.platform == RuntimePlatform.Android)
                deviceAuthorizationManager = new AndroidAuthorizationManager();
            else if (Application.platform == RuntimePlatform.IPhonePlayer)
                deviceAuthorizationManager = new IosAuthorizationManager();
            return deviceAuthorizationManager?.OpenApp(token) ?? Task.CompletedTask;
        }
    }
    
    internal interface IDeviceAuthorizationManager
    {
        public Task OpenApp(CancellationToken token);
    }

    internal class AndroidAuthorizationManager : IDeviceAuthorizationManager
    {
        public Task OpenApp(CancellationToken token)
        {
            return Task.CompletedTask;
        }
    }
    
    internal class IosAuthorizationManager : IDeviceAuthorizationManager
    {
        private TaskCompletionSource<bool> _openAppTask;
        
        public async Task OpenApp(CancellationToken token)
        {
            _openAppTask = new TaskCompletionSource<bool>(token);
            var auth = RequestAuthorization();

            await _openAppTask.Task;
        }
        
        IEnumerator RequestAuthorization()
        {
            const AuthorizationOption authorizationOption = AuthorizationOption.Alert | AuthorizationOption.Badge | AuthorizationOption.Sound;
            using var req = new AuthorizationRequest(authorizationOption, true);
            
            while (!req.IsFinished)
                yield return null;

            _openAppTask?.SetResult(true);
        }
    }
}
