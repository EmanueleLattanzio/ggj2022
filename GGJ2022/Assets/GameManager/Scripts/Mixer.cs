using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using General.Scripts;
using Settings.Scripts;
using UnityEngine;

namespace GameManager.Scripts
{
    public class Mixer : MonoBehaviour, IOpenAppService
    {
        [SerializeField] private SoundsTheme soundsTheme;
        [SerializeField] private float fadeInTime;
        [SerializeField] private float fadeOutTime;
        [SerializeField] private GameObject musicAudioSourcesPrefab;
        [SerializeField] private GameObject effectAudioSourcesPrefab;
        [SerializeField] private List<NamedAudioSource> musicAudioSources;
        [SerializeField] private List<NamedAudioSource> effectAudioSources;
        
        private NamedAudioSource _currentMusic;
        private Coroutine _fadeInCoroutine;
        private SettingsContext _settingsContext;
        private bool MusicIsActive => _settingsContext.Settings?.Music ?? true;
        private bool EffectsAreActive => _settingsContext.Settings?.SoundEffects ?? true;

        public Task OpenApp(CancellationToken token = default)
        {
            _currentMusic = null;
            _settingsContext = (SettingsContext) Services.Get<ContextManager>().Get(SceneName.Settings);
            return Task.CompletedTask;
        }

        public void PlayMusic(string musicName = null)
        {
            if (!MusicIsActive || _currentMusic != null && musicName == _currentMusic.Sound.Name) return;
            var sound = soundsTheme.Music(musicName);
            if (sound == null) return;
            musicName ??= sound.Name;

            NamedAudioSource selectedSource = null;
            foreach (var source in musicAudioSources.Where(source => source.Sound?.Name == musicName || selectedSource == null && !source.AudioSource.isPlaying))
                selectedSource = source; 
            
            if(selectedSource == null)
            {
                var audioSource = Instantiate(musicAudioSourcesPrefab, new Vector3(0, 0, 0), Quaternion.identity, musicAudioSources[0].AudioSource.transform);
                selectedSource = new NamedAudioSource(audioSource.GetComponent<AudioSource>());
                musicAudioSources.Add(selectedSource);
            }

            if (_fadeInCoroutine != null)
                StopCoroutine(_fadeInCoroutine);
            _fadeInCoroutine = StartCoroutine(FadeIn(selectedSource, sound));
        }

        public void Play(string effectName)
        {
            if (!EffectsAreActive) return;
            var sound = soundsTheme.Effect(effectName);
            if (sound == null) return;

            var selectedSource = effectAudioSources.FirstOrDefault(source => !source.AudioSource.isPlaying);

            if (selectedSource == null)
            {
                var audioSource = Instantiate(effectAudioSourcesPrefab, new Vector3(0, 0, 0), Quaternion.identity, effectAudioSources[0].AudioSource.transform);
                selectedSource = new NamedAudioSource(audioSource.GetComponent<AudioSource>());
                effectAudioSources.Add(selectedSource);
            }

            selectedSource.Play(sound);
        }

        public async Task<bool> SetMusicActive(bool isActive)
        {
            var completed = await _settingsContext.SetSettingsPreference(SettingsPreferences.Settings.Music, isActive);
            if (isActive)
                PlayMusic();
            else
                foreach (var source in musicAudioSources)
                    source.AudioSource.Stop();
            return completed;
        }

        public async Task<bool> SetSoundsEffectsActive(bool isActive)
        {
            var completed = await _settingsContext.SetSettingsPreference(SettingsPreferences.Settings.SoundEffects, isActive);
            if (isActive) 
                return completed;
            foreach (var source in effectAudioSources)
                source.AudioSource.Stop();
            return completed;
        }

        private IEnumerator FadeIn (NamedAudioSource source, Sound sound) 
        {
            if (_currentMusic != null)
            {
                var startVolume = _currentMusic.AudioSource.volume;
 
                while (_currentMusic.AudioSource.volume > 0) 
                {
                    _currentMusic.AudioSource.volume -= startVolume * Time.deltaTime / fadeOutTime;
                    yield return null;
                }
 
                _currentMusic.AudioSource.Stop();
                _currentMusic.AudioSource.volume = 0;
            }

            source.Play(sound, true);
            var endVolume = sound.Volume;
            _currentMusic = source;
 
            while (source.AudioSource.volume < endVolume) 
            {
                source.AudioSource.volume += endVolume * Time.deltaTime / fadeInTime;
                yield return null;
            }
 
            source.AudioSource.volume = endVolume;
        }
    }

    [Serializable]
    public class NamedAudioSource
    {
        public Sound Sound { get; private set; }

        [SerializeField] private AudioSource source;
        public AudioSource AudioSource => source;

        public NamedAudioSource(AudioSource audioSource)
        {
            source = audioSource;
        }

        public void Play(Sound playSound, bool fadeIn = false)
        {
            if (Sound?.Name != playSound.Name)
            {
                Sound = playSound;
                source.volume = fadeIn ? 0 : Sound.Volume;
                source.clip = Sound.AudioClip;
            }
            source.Play();
        }
    }
}