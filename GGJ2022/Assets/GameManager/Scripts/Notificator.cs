using System;
using System.Threading;
using System.Threading.Tasks;
using Settings.Scripts;
using Unity.Notifications.Android;
using Unity.Notifications.iOS;
using UnityEngine;

namespace GameManager.Scripts
{
    public class Notificator : ScriptableObject, IOpenAppService
    {
        public const int NotOpenAppNotificationID = 10010;
        public const string NotOpenAppNotificationTitle = "Il tempo passa";
        public const string NotOpenAppNotificationSubTitle = "Senza mai fermarsi";
        public const string NotOpenAppNotificationText = "Perchè non torni a giocare?";

        [SerializeField] private int daysWithoutOpenApp;
        private IDeviceNotificator _deviceNotificator;
        
        public Task OpenApp(CancellationToken token = default)
        {
            if (daysWithoutOpenApp <= 0)
                return Task.CompletedTask;

            if (Application.platform == RuntimePlatform.Android)
                _deviceNotificator = new AndroidNotificator();
            else if (Application.platform == RuntimePlatform.IPhonePlayer)
            {
                if (iOSNotificationCenter.GetNotificationSettings().NotificationCenterSetting != NotificationSetting.Enabled)
                    return Task.CompletedTask;
                _deviceNotificator = new IosNotificator();
            }
            
            _deviceNotificator?.OpenApp(daysWithoutOpenApp);
            return Task.CompletedTask;
        }
        
        public Task<bool> SetNotificationsActive(bool isActive)
        {
            return ((SettingsContext)Services.Get<ContextManager>().Get(SceneName.Settings)).SetSettingsPreference(
                SettingsPreferences.Settings.Notifications, isActive);
        }
    }

    internal interface IDeviceNotificator
    {
        public void OpenApp(int daysWithoutOpenApp);
    }
    
    internal class AndroidNotificator: IDeviceNotificator
    {
        private const string ChannelId = "GGJ2022";
        private const string ChannelName = "GGJ2022_Channel";
        private const string ChannelDescription = "GGJ2022 notifications";
        private const Importance ChannelImportance = Importance.Default;

        private const string DefaultNotificationSmallIcon = "GGJ2022_icon_id";
        private const string DefaultNotificationLargeIcon = "GGJ2022_large_icon_id";
        
        internal AndroidNotificator()
        {
            var channel = new AndroidNotificationChannel()
            {
                Id = ChannelId,
                Name = ChannelName,
                Importance = ChannelImportance,
                Description = ChannelDescription
            };
            AndroidNotificationCenter.RegisterNotificationChannel(channel);
        }

        public void OpenApp(int daysWithoutOpenApp)
        {
            var notificationStatus = AndroidNotificationCenter.CheckScheduledNotificationStatus(Notificator.NotOpenAppNotificationID);
            
            var newNotification = new AndroidNotification
            {
                Title = Notificator.NotOpenAppNotificationTitle,
                Text = Notificator.NotOpenAppNotificationText,
                FireTime = DateTime.Now.AddMinutes(daysWithoutOpenApp),
                SmallIcon = DefaultNotificationSmallIcon,
                LargeIcon = DefaultNotificationLargeIcon,
            };
            
            if (notificationStatus == NotificationStatus.Scheduled)
            {
                AndroidNotificationCenter.UpdateScheduledNotification(Notificator.NotOpenAppNotificationID, newNotification, ChannelId);
            }
            else if (notificationStatus == NotificationStatus.Delivered)
            {
                AndroidNotificationCenter.CancelNotification(Notificator.NotOpenAppNotificationID);
                AndroidNotificationCenter.SendNotification(newNotification, ChannelId);
            }
            else if (notificationStatus == NotificationStatus.Unknown)
            {
                AndroidNotificationCenter.SendNotification(newNotification, ChannelId);
            }
        }
    }

    internal class IosNotificator : IDeviceNotificator
    {
        private const string NotOpenAppNotificationCategory = "category_a";
        private const string NotOpenAppNotificationThread = "thread1";
        private string NotOpenAppNotificationID => $"_notification_{Notificator.NotOpenAppNotificationID}";

        public void OpenApp(int daysWithoutOpenApp)
        {
            iOSNotificationCenter.RemoveScheduledNotification(NotOpenAppNotificationID);
            iOSNotificationCenter.RemoveDeliveredNotification(NotOpenAppNotificationID);

            var timeTrigger = new iOSNotificationTimeIntervalTrigger
            {
                TimeInterval = new TimeSpan(daysWithoutOpenApp * 24, 0, 0),
                Repeats = false
            };

            var notification = new iOSNotification
            {
                Identifier = NotOpenAppNotificationID,
                Title = Notificator.NotOpenAppNotificationTitle,
                Body = Notificator.NotOpenAppNotificationText,
                Subtitle = Notificator.NotOpenAppNotificationSubTitle,
                ShowInForeground = true,
                ForegroundPresentationOption = PresentationOption.Alert | PresentationOption.Sound,
                CategoryIdentifier = NotOpenAppNotificationCategory,
                ThreadIdentifier = NotOpenAppNotificationThread,
                Trigger = timeTrigger,
            };

            iOSNotificationCenter.ScheduleNotification(notification);
        }
    }
}
