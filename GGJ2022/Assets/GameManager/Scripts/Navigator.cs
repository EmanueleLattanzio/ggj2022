using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using General.Scripts;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace GameManager.Scripts
{
    [Serializable]
    public enum SceneName
    {
        GameCore, HomePage, Settings
    }

    [Serializable]
    public class Navigator : ScriptableObject, IOpenAppService, IDestroyService 
    {
        [SerializeField] private int maxCachedPages;
        [SerializeField] private int maxBackPages;
        [SerializeField] private SceneName[] popupScenes;
        private List<Scene> _loadedScenes = new List<Scene>();
        private List<SceneName> _history = new List<SceneName>();

        public Task OpenApp(CancellationToken token = default)
        {
            SceneManager.sceneLoaded += OnSceneAdded;
            _loadedScenes.Clear();
            _history.Clear();
            return Task.CompletedTask;
        }
        
        public async void Open(SceneName sceneName)
        {
            var currentScene = SceneManager.GetActiveScene();
            var newScene = SceneManager.GetSceneByName(sceneName.ToString());

            if (currentScene == newScene)
                return;
            
            var newContextInit = Services.Get<ContextManager>().Init(sceneName);
            
            if (_loadedScenes.Any())
            {
                if (!popupScenes.Contains(sceneName))
                {
                    var sceneController = currentScene.GetRootGameObjects().FirstOrDefault(x => x.name == currentScene.name)?.GetComponent<ICloseController>();
                    if (sceneController == null)
                        throw new KeyNotFoundException();
                    await sceneController.Close();
                }
                
                var currentSceneName = (SceneName)Enum.Parse(typeof(SceneName), currentScene.name);
                if (!popupScenes.Contains(currentSceneName))
                {
                    if(_history.Count == 0 || _history.Last() != currentSceneName)
                        _history.Add(currentSceneName);
                    if (_history.Count > maxBackPages)
                        _history.Remove(0);
                }
            }

            await newContextInit;
            
            if (newScene.isLoaded)
            {
                SceneManager.SetActiveScene(newScene);
                newScene.GetRootGameObjects().FirstOrDefault(x => x.name == sceneName.ToString())?.SetActive(true);
                _loadedScenes.Remove(newScene);
                _loadedScenes.Add(newScene);
            }
            else
                SceneManager.LoadSceneAsync(sceneName.ToString(), LoadSceneMode.Additive);
        }

        public void Back(int pages = 1)
        {
            if (_history.Count == 0)
                return;
            if (_history.Count < pages)
                pages = _history.Count;
            var backIndex = _history.Count - pages;
            var last = _history[backIndex];
            for (var i = _history.Count - 1; i >= backIndex; i--)
                _history.RemoveAt(i);
            Open(last);
        }

        private void OnSceneAdded(Scene scene, LoadSceneMode sceneMode)
        {
            SceneManager.SetActiveScene(scene);
            _loadedScenes.Add(scene);
            if (maxCachedPages <= 0 || popupScenes.Contains((SceneName)Enum.Parse(typeof(SceneName), scene.name)))
                return;

            while (_loadedScenes.Count > maxCachedPages)
            {
                var last = _loadedScenes[0];
                SceneManager.UnloadSceneAsync(last);
                _loadedScenes.Remove(last);
            }
        }

        public void OnDestroy()
        {
            SceneManager.sceneLoaded -= OnSceneAdded;
        }
    }
}