using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using UnityEngine;

namespace GameManager.Scripts
{
    public class ServiceManager: MonoBehaviour
    {
        [SerializeField] private UnityEngine.Object[] services;

        public Task Init(CancellationToken token = default)
        {
            foreach (var s in services)
            {
                if (s is IService service)
                    Services.Add(s.GetType(), service);
                else if (s is GameObject objectService)
                {
                    var objService = objectService.GetComponent<IService>();
                    Services.Add(objService.GetType(), objService);
                }
                else
                    throw new Exception($"{s.name} is not a Service");
            }
            return Services.InitAll(token);
        }

        private void OnDestroy()
        {
            foreach (var s in services)
                if (s is IDestroyService service)
                    service.OnDestroy();
        }
    }

    public static class Services
    {
        private static readonly Dictionary<Type, IService> TypedServices = new();

        internal static void Add(Type type, IService service)
        {
            TypedServices[type] = service;
        }

        internal static async Task InitAll(CancellationToken token = default)
        {
            foreach (var s in TypedServices.Values)
                if (s is IOpenAppService openAppService)
                    await openAppService.OpenApp(token);
        }

        public static T Get<T>()
        {
            if (TypedServices.TryGetValue(typeof(T), out var service))
                return (T)service;
            throw new ArgumentException($"No Service of type {typeof(T).Name} found");
        }
    }

    public interface IService
    {
        
    }

    public interface IOpenAppService : IService
    {
        public Task OpenApp(CancellationToken token = default);
    }

    public interface IDestroyService : IService
    {
        public void OnDestroy();
    }
}