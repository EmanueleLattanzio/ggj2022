using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using UnityEngine;

namespace GameManager.Scripts
{
    public class ContextManager : ScriptableObject, IOpenAppService
    {
        [SerializeField] private SceneContext[] openAppContexts;
        [SerializeField] private SceneContext[] commonContexts;
        
        public async Task OpenApp(CancellationToken token = default)
        {
            foreach (var c in openAppContexts)
                c.Reset();
            foreach (var c in commonContexts)
                c.Reset();

            foreach (var openAppContext in openAppContexts)
            {
                await openAppContext.UpdateContext();
            }
        }
        
        public Task Init(SceneName sceneName)
        {
            var ctx = openAppContexts.FirstOrDefault(x => x.SceneName == sceneName);
            if (ctx == null)
                ctx = commonContexts.FirstOrDefault(x => x.SceneName == sceneName);
            return ctx == null ? Task.CompletedTask : ctx.UpdateContext();
        }

        public SceneContext Get(SceneName sceneName)
        {
            var context = openAppContexts.FirstOrDefault(x => x.SceneName == sceneName);
            if (context == null)
                context = commonContexts.FirstOrDefault(x => x.SceneName == sceneName);
            if (context != null && !context.IsUpdated)
                throw new Exception("Not updated context");
            return context;
        }
    }

    public abstract class SceneContext : ScriptableObject
    {
        [SerializeField] private SceneName sceneName;    
        private CancellationTokenSource _cts; 
        private bool _updated;
        private Task _updateContext;
        public bool IsUpdated => _updated; 
        public SceneName SceneName => sceneName;

        internal void Reset()
        {
            _updated = false;
            _updateContext = null;
        }

        public async Task UpdateContext(bool force = false)
        {
            if (!_updated || force)
            {
                _updated = false;
                _cts ??= new CancellationTokenSource();
                try
                {
                    if (_updateContext == null) 
                    {
                        _updateContext = DoUpdateContext(_cts.Token);
                
                        await _updateContext;
                   
                        _updateContext = null;
                        _updated = true;
                    }
                    else
                        await _updateContext; 
                }
                catch (TaskCanceledException)
                {
                    Debug.Log($"{name}: UpdateContext cancelled");
                }
                finally
                {
                    _cts?.Dispose();
                }
                _cts = null;
            }
        }

        // don't call this internally in others SceneContext but only with this UpdateContext
        protected virtual async Task DoUpdateContext(CancellationToken token = default)
        {
            await Task.CompletedTask;
        }

        private void OnDestroy()
        {
            _cts?.Cancel();
        }
    }
}
