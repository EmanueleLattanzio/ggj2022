using System;
using System.Threading;
using System.Threading.Tasks;
using UnityEngine;

namespace GameManager.Scripts
{
    [Serializable]
    public class UserManager : ScriptableObject, IOpenAppService
    {
        private string _deviceId;
        public string DeviceId
        {
            get {
                if (string.IsNullOrEmpty(_deviceId))
                    _deviceId = SystemInfo.deviceUniqueIdentifier;

                return _deviceId;
            }
        }

        public Task OpenApp(CancellationToken token = default)
        {
            return Task.CompletedTask;
        }
    }
}