using System.Threading;
using System.Threading.Tasks;
using Settings.Scripts;
using UnityEngine;

namespace GameManager.Scripts
{
    public class Vibrator : ScriptableObject, IOpenAppService
    {
        [SerializeField] private int timeout;
        private SettingsContext _settingsContext;
        
        public Task OpenApp(CancellationToken token = default)
        {
            _settingsContext = (SettingsContext)Services.Get<ContextManager>().Get(SceneName.Settings);
            return Task.CompletedTask;
        }
        
        public async Task Vibrate(int times = 1)
        {
            if (!_settingsContext.Settings.Vibrations)
                return;
            
            for (var i = 0; i < times; i++)
            {
                Handheld.Vibrate();
                await Task.Delay(timeout);
            }
        }
        
        public Task<bool> SetVibrationActive(bool isActive)
        {
            return _settingsContext.SetSettingsPreference(SettingsPreferences.Settings.Vibrations, isActive);
        }
    }
}
