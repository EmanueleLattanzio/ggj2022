using System;
using GameManager.Scripts;
using General.Scripts;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Settings.Scripts
{
    public class LanguageChoiceButton : MonoBehaviour
    {
        [SerializeField] private TextMeshProUGUI text;
        private SettingsContext _settingsContext;
        
        private void Start()
        {
            if (_settingsContext == null)
                _settingsContext = (SettingsContext) Services.Get<ContextManager>().Get(SceneName.Settings);

            OnLanguageChanged();
            Events.LanguageChanged += OnLanguageChanged;
        }

        private void OnLanguageChanged()
        {
            text.text = _settingsContext.Settings.Language.ToString();
        }

        private void OnDestroy()
        {
            Events.LanguageChanged -= OnLanguageChanged;
        }
    }
}
