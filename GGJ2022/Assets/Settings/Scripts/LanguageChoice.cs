using System;
using GameManager.Scripts;
using General.Scripts;
using UnityEngine;
using UnityEngine.UI;

namespace Settings.Scripts
{
    [Serializable]
    public class LanguageButton
    {
        public SettingsPreferences.Languages language;
        public Image button;
    }
    
    public class LanguageChoice : MonoBehaviour
    {
        [SerializeField] private Color selectedButtonColor, unselectedButtonColor;
        [SerializeField] private LanguageButton[] languages;
        private SettingsContext _settingsContext;
        
        private void OnEnable()
        {
            if (_settingsContext == null)
                _settingsContext = (SettingsContext) Services.Get<ContextManager>().Get(SceneName.Settings);

            foreach (var l in languages)
                l.button.color = l.language == _settingsContext.Settings.Language ? selectedButtonColor : unselectedButtonColor;
        }

        public async void ChangeLanguage(string language)
        {
            var newLanguage = Enum.Parse<SettingsPreferences.Languages>(language);
            if(_settingsContext.Settings.Language == newLanguage)
                return;

            await _settingsContext.SetLanguagesPreference(newLanguage);
            
            foreach (var l in languages)
                l.button.color = l.language == newLanguage ? selectedButtonColor : unselectedButtonColor;

            Events.OnLanguageChanged();
        }
    }
}
