using System;
using System.Threading;
using System.Threading.Tasks;
using GameManager.Scripts;
using UnityEngine;

namespace Settings.Scripts
{
    public class SettingsContext : SceneContext
    {
        private string UserDataSettingsPreferences => $"{Services.Get<UserManager>().DeviceId}_UserData_SettingsPreferences_";
        
        public SettingsPreferences Settings { get; private set; }
        
        protected override async Task DoUpdateContext(CancellationToken token = default)
        {
            await GetSettingsPreferences();
        }
        
        private Task GetSettingsPreferences()
        {
            Settings = new SettingsPreferences(
                PlayerPrefs.GetInt($"{UserDataSettingsPreferences}{SettingsPreferences.Settings.Music.ToString()}", 1) > 0,
                PlayerPrefs.GetInt($"{UserDataSettingsPreferences}{SettingsPreferences.Settings.SoundEffects.ToString()}", 1) > 0,
                PlayerPrefs.GetInt($"{UserDataSettingsPreferences}{SettingsPreferences.Settings.Vibrations.ToString()}", 1) > 0,
                PlayerPrefs.GetInt($"{UserDataSettingsPreferences}{SettingsPreferences.Settings.Notifications.ToString()}", 1) > 0,
                Enum.Parse<SettingsPreferences.Languages>(PlayerPrefs.GetString($"{UserDataSettingsPreferences}{SettingsPreferences.Settings.Language.ToString()}", SettingsPreferences.Languages.English.ToString()))
            );
            
            return Task.CompletedTask;
        }
        
        public Task<bool> SetSettingsPreference(SettingsPreferences.Settings setting, bool value)
        {
            PlayerPrefs.SetInt($"{UserDataSettingsPreferences}{setting.ToString()}", value ? 1 : 0);
            Settings.UpdateSettingsPreference(setting, value);
            
            return Task.FromResult(true);
        }
        
        public Task<bool> SetLanguagesPreference(SettingsPreferences.Languages value)
        {
            PlayerPrefs.SetString($"{UserDataSettingsPreferences}{SettingsPreferences.Settings.Language.ToString()}", value.ToString());
            Settings.UpdateLanguagesPreference(value);
            
            return Task.FromResult(true);
        }
    }
    
    public class SettingsPreferences
    {
        [Serializable]
        public enum Settings
        {
            Music, SoundEffects, Vibrations, Notifications, Language
        }
        
        [Serializable]
        public enum Languages
        {
            English, Italiano
        }

        internal SettingsPreferences(bool music, bool soundEffects, bool vibrations, bool notifications, Languages language)
        {
            Music = music;
            SoundEffects = soundEffects;
            Vibrations = vibrations;
            Notifications = notifications;
            Language = language;
        }

        public bool IsActive(Settings setting)
        {
            return setting switch
            {
                Settings.Music => Music,
                Settings.SoundEffects => SoundEffects,
                Settings.Vibrations => Vibrations,
                Settings.Notifications => Notifications,
                _ => throw new ArgumentOutOfRangeException(nameof(setting), setting, null)
            };
        }
        
        internal void UpdateSettingsPreference(Settings setting, bool value)
        {
            switch (setting)
            {
                case Settings.Music:
                    Music = value;
                    break;
                case Settings.SoundEffects:
                    SoundEffects = value;
                    break;
                case Settings.Vibrations:
                    Vibrations = value;
                    break;
                case Settings.Notifications:
                    Notifications = value;
                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(setting), setting, null);
            }
        }
        
        internal void UpdateLanguagesPreference(Languages language)
        {
            Language = language;
        }
        
        public bool Music  { get; private set; }
        public bool SoundEffects  { get; private set; }
        public bool Vibrations  { get; private set; }
        public bool Notifications  { get; private set; }
        public Languages Language  { get; private set; }
    }
}
