using System;
using TMPro;
using UnityEngine;

namespace GameCore.Scripts
{
    public class Player : MonoBehaviour
    {
        [SerializeField] private TileType tileType;
        public TileType TileType => tileType;
    
        [SerializeField] private int points;
        public int Points { get => points; set => points = value; }

        private int _finalPoints;
        public int FinalPoints { get => _finalPoints; set => _finalPoints = value; }

        [SerializeField] private TextMeshProUGUI pointsText;
    
        [SerializeField] private TileTypeCount[] resources;
        public TileTypeCount[] Resources => resources;
    
        [SerializeField] private Player nextPlayer;
        public Player NextPlayer => nextPlayer;

        private void OnEnable()
        {
            foreach (var resource in resources)
                resource.button.Init(tileType, resource.level);
        }

        public void UpdateCounters()
        {
            pointsText.text = $"{points + _finalPoints}"; //$"{points}{(_finalPoints == 0 ? "" : $" + {_finalPoints}")}";
            foreach (var resource in resources)
                resource.countText.text = "x " + resource.Count;
        }

        public void SetEnableCommands(bool isActive)
        {
            foreach (var resource in resources)
                resource.button.button.interactable = isActive && resource.Count > 0;
        }
    }

    [Serializable]
    public class TileTypeCount
    {
        public int level;
        [SerializeField] private int startCount;
        private int _count;
        public int Count { get => _count; set => _count = value; }
        public TileTypeButton button;
        public TextMeshProUGUI countText;

        public void Init()
        {
            _count = startCount;
        }
    }
}