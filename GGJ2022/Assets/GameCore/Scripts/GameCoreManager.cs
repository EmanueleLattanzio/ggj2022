using System;
using System.Collections.Generic;
using System.Linq;
using GameCore.Scripts;
using TMPro;
using UnityEngine;

namespace GameCore.Scripts
{
    public class GameCoreManager : MonoBehaviour
    {
        private static readonly int EndGame = Animator.StringToHash("EndGame");
        [SerializeField] private Animator animator;
        [SerializeField] private List<Tile> tiles;
        [SerializeField] private List<Player> players;
        [SerializeField] private GameObject resultPopup;
        [SerializeField] private VictoryData[] victoryData;
        [SerializeField] private Referee referee;

        private void OnEnable()
        {
            transform.position = Vector3.zero;
            resultPopup.SetActive(false);
            foreach (var vd in victoryData)
            {
                vd.crown.SetActive(false);
                vd.points.text = "0";
            }
            
            foreach (var player in players)
            {
                player.gameObject.SetActive(true);
                player.Points = 0;
                player.FinalPoints = 0;
                player.SetEnableCommands(false);
                foreach (var resource in player.Resources)
                    resource.Init();
                player.UpdateCounters();
            }

            foreach (var tile in tiles)
                tile.SetType(TileType.None, 0); 

            referee.Init(tiles, players, this);
            referee.StartMatch();
        }

        public void SetWinner(Player winner)
        {
            animator.SetTrigger(EndGame);
            foreach (var vd in victoryData)
            {
                vd.crown.SetActive(vd.tileType == winner.TileType);
                var player = players.FirstOrDefault(p => p.TileType == vd.tileType);
                if (player != null)
                    vd.points.text = (player.Points + player.FinalPoints).ToString();
            }
            resultPopup.SetActive(true);
        }
    }
}

[Serializable]
public class VictoryData
{
    public TileType tileType;
    public GameObject crown;
    public TextMeshProUGUI points;
}
