using System;
using System.Collections.Generic;
using UnityEngine;

namespace GameCore.Scripts
{
    [Serializable]
    public enum TileType
    {
        None, Nature, City
    }

    public class Tile : MonoBehaviour
    {
        public TileType type;
        public int level;
        public TileType[] defaultType;
        public List<Tile> nearTails;
        public MeshRenderer defaultRenderer;
        public TileObject[] tilesObj;
        public GameObject selection;
        private bool _clickable;
        public Referee referee;

        private void Start()
        {
            SetType(TileType.None, 0);
            SetSelectable(false);
        }

        public void SetSelectable(bool isSelectable)
        {
            selection.SetActive(isSelectable);
            _clickable = isSelectable;
        }
    
        public void SetType(TileType tileType, int tileLevel)
        {
            defaultRenderer.enabled = tileType == TileType.None;
            foreach (var obj in tilesObj)
                obj.obj.SetActive(obj.type == tileType && obj.level == tileLevel);
            type = tileType;
            level = tileLevel;
        }

        private void OnMouseDown()
        {
            if(_clickable)
                referee.PlaySelectedTileType(this);
        }
    }

    [Serializable]
    public class TileObject
    {
        public TileType type;
        public int level;
        public GameObject obj;
    }
}