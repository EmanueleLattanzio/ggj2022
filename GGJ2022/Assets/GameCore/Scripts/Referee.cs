using System;
using System.Collections.Generic;
using System.Linq;
using GameManager.Scripts;
using UnityEngine;

namespace GameCore.Scripts
{
    public class Referee : ScriptableObject, IService
    {
        private List<Tile> _tiles;
        private List<Player> _players;
        private GameCoreManager _gameCoreManager;
        private Player _currentPlayer;
        private TileType _selectedTileType;
        private int _selectedTileLevel;

        public void Init(List<Tile> gameTiles, List<Player> gamePlayers, GameCoreManager gameCoreManager)
        {
            _tiles = gameTiles;
            _players = gamePlayers;
            _selectedTileType = TileType.None;
            _selectedTileLevel = 0;
            _gameCoreManager = gameCoreManager;
        }

        public void StartMatch()
        {
            SetPlayerTurn(_players[0]); // TODO random?
        }
        
        private void SetPlayerTurn(Player player)
        {
            if (!player.Resources.Any(r => r.Count > 0 && _tiles.Any(t => CheckTileAssigment(t, player.TileType, r.level))))
                EndMatch(_currentPlayer);
            _currentPlayer = player;
            _currentPlayer.SetEnableCommands(true);
            foreach (var tile in _tiles)
                tile.SetSelectable(false);
        }

        public void SelectTileType(TileType tileType, int tileLevel)
        {
            if (_selectedTileType == tileType && _selectedTileLevel == tileLevel)
            {
                _selectedTileType = TileType.None;
                _selectedTileLevel = 0;
                foreach (var tile in _tiles)
                    tile.SetSelectable(false);
                return;
            }
        
            _selectedTileType = tileType;
            _selectedTileLevel = tileLevel;
            foreach (var tile in _tiles)
            {
                bool isAssignable = CheckTileAssigment(tile, tileType, tileLevel);
                tile.SetSelectable(isAssignable);
            }
            Services.Get<Mixer>().Play("TileSulTavolo");
        }

        private bool CheckTileAssigment(Tile tile, TileType tileType, int tileLevel)
        {
            return tile.type == TileType.None && (tile.defaultType.Contains(tileType) ||
                                                  tile.nearTails.Any(t => t.type == tileType)) ||
                   tile.type != TileType.None && tile.type != tileType && tile.level <= tileLevel &&
                   tile.nearTails.Any(t => t.type == tileType && t.level == tileLevel);
        }
    
        public void PlaySelectedTileType(Tile selectedTile)
        {
            if (_currentPlayer.TileType == _selectedTileType &&
                _currentPlayer.Resources.Any(r => r.level == _selectedTileLevel && r.Count > 0) &&
                CheckTileAssigment(selectedTile, _selectedTileType, _selectedTileLevel))
            {
                if (selectedTile.type != TileType.None)
                {
                    _currentPlayer.Points += 1;
                    Services.Get<Mixer>().Play($"{_currentPlayer.TileType}Point");
                }
                else
                    Services.Get<Mixer>().Play("TileSulTavolo");
                _currentPlayer.Resources.First(x => x.level == _selectedTileLevel).Count --;
                selectedTile.SetType(_selectedTileType, _selectedTileLevel);
                var winner = UpdatePoints();
                _currentPlayer.SetEnableCommands(false);

                if (_players.All(p => p.Resources.All(r => r.Count == 0)))
                    EndMatch(winner);
                else
                    SetPlayerTurn(_currentPlayer.NextPlayer);
            }
        }

        private Player UpdatePoints()
        {
            Player winner = _players[1];
            foreach (var tileType in Enum.GetValues(typeof(TileType)).Cast<TileType>())
            {
                var player = _players.FirstOrDefault(p => p.TileType == tileType);
                if (player != null)
                {
                    player.FinalPoints = _tiles.Count(tile =>
                        tile.type == tileType && tile.defaultType.Any(dt => dt != tileType && dt != TileType.None));
                    player.UpdateCounters();
                    if (player.Points + player.FinalPoints > winner.Points + winner.FinalPoints)
                        winner = player;
                }
            }

            return winner;
        }
        
        private async void EndMatch(Player winner)
        {
            foreach (var tile in _tiles)
                tile.SetSelectable(false);
            Services.Get<Mixer>().Play("EndSound");
            _gameCoreManager.SetWinner(winner);
            await Services.Get<Vibrator>().Vibrate(3);
        }
    }
}
