using UnityEngine;
using UnityEngine.UI;

namespace GameCore.Scripts
{
    public class TileTypeButton : MonoBehaviour
    {
        private TileType _type;
        private int _level;
        public Button button;
        public Referee referee;

        public void Init(TileType tileType, int tileLevel)
        {
            _type = tileType;
            _level = tileLevel;
        }
        
        public void SelectTileType()
        {
            referee.SelectTileType(_type, _level);
        }
    }
}
