using System;
using Settings.Scripts;

namespace General.Scripts
{
    public static class Events
    {
        public static event Action LanguageChanged;

        public static void OnLanguageChanged()
        {
            LanguageChanged?.Invoke();
        }
    }
}
