using System;
using System.Linq;
using UnityEngine;

namespace General.Scripts
{
    [Serializable]
    public class SoundsTheme : ScriptableObject
    {
        [SerializeField] private Sound[] musics;
        [SerializeField] private Sound[] effects;

        public Sound Effect(string effectName) => effects.First(clip => clip.Name == effectName);
        public Sound Music(string musicName) => string.IsNullOrEmpty(musicName) ? musics[0] : musics.First(clip => clip.Name == musicName);
    }

    [Serializable]
    public class Sound
    {
        [SerializeField] private string name;
        public string Name => name;

        [SerializeField] private AudioClip audioClip;
        public AudioClip AudioClip => audioClip;

        [SerializeField, Range(0f, 1f)] private float volume;
        public float Volume => volume;
    }
}