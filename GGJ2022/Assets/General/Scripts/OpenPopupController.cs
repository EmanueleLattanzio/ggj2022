using System;
using System.Threading.Tasks;
using UnityEngine;

namespace General.Scripts
{
    public class OpenPopupController : MonoBehaviour, ICloseController
    {
        private static readonly int CloseTrigger = Animator.StringToHash("Close");
        [Serializable] private enum OpenDirectionType { Center, Top, Right, Bottom, Left }
    
        [SerializeField] private OpenDirectionType openDirection;
        [SerializeField] private Animator animator;
        
        private TaskCompletionSource<bool> _taskCompletionSource;

        private void OnEnable()
        {
            animator.SetTrigger(openDirection.ToString());
        }

        public void ClosePopup()
        {
            Close();
        }

        public Task Close()
        {
            animator.SetTrigger(CloseTrigger);
            _taskCompletionSource = new TaskCompletionSource<bool>();
            return _taskCompletionSource.Task;
        }

        public void Exit()
        {
            gameObject.SetActive(false);
            _taskCompletionSource?.SetResult(true);
        }
    }
}
