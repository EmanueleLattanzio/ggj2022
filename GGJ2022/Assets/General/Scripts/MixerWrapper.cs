using System;
using GameManager.Scripts;
using UnityEngine;

namespace General.Scripts
{
    [Serializable]
    public class MixerWrapper : ScriptableObject
    {
        public void Play(string effectName)
        {
            Services.Get<Mixer>().Play(effectName);
        }

        public void PlayMusic(string musicName)
        {
            Services.Get<Mixer>().PlayMusic(musicName);
        }

        public void SetActive(bool isActive)
        {
            Services.Get<Mixer>().SetMusicActive(isActive);
            Services.Get<Mixer>().SetSoundsEffectsActive(isActive);
        }
    }
}