using System.Threading.Tasks;
using UnityEngine;

namespace General.Scripts
{
    public class ScenePopupController : MonoBehaviour, ICloseController
    {
        [SerializeField] private OpenPopupController popupController;

        private void OnEnable()
        {
            popupController.gameObject.SetActive(true);
        }
        
        public async Task Close()
        {
            await popupController.Close();
            Exit();
        }

        public void Exit()
        {
            gameObject.SetActive(false);
        }
    }
}
