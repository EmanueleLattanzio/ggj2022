using System.Threading.Tasks;
using GameManager.Scripts;
using UnityEngine;

namespace General.Scripts
{
    public class SceneController : MonoBehaviour, ICloseController
    {
        private static readonly int Open = Animator.StringToHash("Open");
        private static readonly int CloseTrigger = Animator.StringToHash("Close");
        
        [SerializeField] private Animator animator;
        [SerializeField] private string music;
        private TaskCompletionSource<bool> _taskCompletionSource;

        private void OnEnable()
        {
            if (music != null)
                Services.Get<Mixer>().PlayMusic(music);
            animator.SetTrigger(Open);
        }
        
        public Task Close()
        {
            animator.SetTrigger(CloseTrigger);
            _taskCompletionSource = new TaskCompletionSource<bool>();
            return _taskCompletionSource.Task;
        }

        // Is called from animator
        public void Exit()
        {
            _taskCompletionSource?.SetResult(true);
            gameObject.SetActive(false);
        }
    }
}
