﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

namespace General.Scripts
{
    public class ScrollViewSwipe : MonoBehaviour
    {
        [SerializeField] private Color selectedBtnColor, unselectedBtnColor;
        [SerializeField] private Transform buttonsContent;
        [SerializeField] private Scrollbar scrollbar;
        private Transform[] _children;
        private List<Transform> _buttons;
        private int _buttonsCount;
        private float _scrollPos;
        private float[] _pos;
        private bool _runIt;
        private float _time;
        private int _btnNumber;
        private float _btnWidth;

        private void Start()
        {
            _children = (from object child in transform select ((RectTransform)child).transform).ToArray();
            _buttons = (from object child in buttonsContent select ((RectTransform)child).transform).ToList();
            _buttonsCount = _buttons.Count;
            _btnWidth = ((RectTransform)_buttons[0]).rect.width;
        }

        private void Update()
        {
            
            _pos = new float[_buttonsCount];
            var distance = 1f / (_buttonsCount - 1f);

            if (_runIt)
            {
                UpdateScrollPosition(distance, _pos);
                _time += Time.deltaTime;

                if (_time > 1f)
                {
                    _time = 0;
                    _runIt = false;
                }
            }

            for (var i = 0; i < _buttonsCount; i++)
                _pos[i] = distance * i;

            if (Input.GetMouseButton(0))
                _scrollPos = scrollbar.value;
            else
                foreach (var t in _pos)
                    if (_scrollPos < t + distance / 2 && _scrollPos > t - distance / 2)
                        scrollbar.value = Mathf.Lerp(scrollbar.value, t, 0.1f);


            for (var i = 0; i < _buttonsCount; i++)
            {
                if (_scrollPos < _pos[i] + distance / 2 && _scrollPos > _pos[i] - distance / 2)
                {
                    _children[i].localScale = Vector2.Lerp(_children[i].localScale, new Vector2(1f, 1f), 0.1f);
                    ((RectTransform)_buttons[i]).sizeDelta = Vector2.Lerp(((RectTransform)_buttons[i]).sizeDelta, _btnWidth * new Vector2(1.1f, 1.1f), 0.1f);
                    _buttons[i].gameObject.GetComponent<Image>().color = selectedBtnColor;
                }
                else
                {
                    _buttons[i].gameObject.GetComponent<Image>().color = unselectedBtnColor;
                    ((RectTransform)_buttons[i]).sizeDelta = Vector2.Lerp(((RectTransform)_buttons[i]).sizeDelta, _btnWidth * new Vector2(0.9f, 0.9f), 0.1f);
                    _children[i].localScale = Vector2.Lerp(_children[i].localScale, new Vector2(0.8f, 0.8f), 0.1f);
                }
            }
        }

        private void UpdateScrollPosition(float distance, IReadOnlyList<float> pos)
        {
            foreach (var t in pos)
                if (_scrollPos < t + distance / 2 && _scrollPos > t - distance / 2)
                    scrollbar.value = Mathf.Lerp(scrollbar.value, pos[_btnNumber], 1f * Time.deltaTime);
        }
        
        public void WhichBtnClicked(Button btn)
        {
            _btnNumber = _buttons.IndexOf(btn.transform);
            _time = 0;
            _scrollPos = _pos[_btnNumber];
            _runIt = true;
        }
    }
}