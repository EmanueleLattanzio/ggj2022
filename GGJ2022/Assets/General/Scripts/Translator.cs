using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using GameManager.Scripts;
using Settings.Scripts;
using UnityEngine;

namespace General.Scripts
{
    [Serializable]
    public class Translations
    {
        [SerializeField] private string id;
        [SerializeField] private Translation[] translations;

        public string Id => id;
        public string GetText(SettingsPreferences.Languages language)
        {
            return translations.FirstOrDefault(x => x.Language == language)?.Text;
        }
    }
    
    [Serializable]
    public class Translation
    {
        [SerializeField] private SettingsPreferences.Languages language;
        public SettingsPreferences.Languages Language => language;
        [SerializeField] private string text;
        public string Text => text;
    }

    [Serializable]
    public class Translator : ScriptableObject, IOpenAppService
    {
        [SerializeField] private Translations[] translations;
        private SettingsContext _settingsContext;

        public string GetTranslation(string id) => translations.FirstOrDefault(x => x.Id == id)?.GetText(_settingsContext.Settings.Language);
        public string GetTranslation(string id, SettingsPreferences.Languages language) => translations.FirstOrDefault(x => x.Id == id)?.GetText(language);
        public Task OpenApp(CancellationToken token = default)
        {
            _settingsContext = (SettingsContext) Services.Get<ContextManager>().Get(SceneName.Settings);
            return Task.CompletedTask;
        }
    }

}