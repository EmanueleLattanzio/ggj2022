using System;
using GameManager.Scripts;
using Settings.Scripts;
using TMPro;
using UnityEngine;

namespace General.Scripts
{
    public class LocalizationText : MonoBehaviour
    {
        [SerializeField] private TextMeshProUGUI text;
        [SerializeField] private string textId;
    
        private void Start()
        {
            Events.LanguageChanged += OnLanguageChanged;
            OnLanguageChanged();
        }

        private void OnLanguageChanged()
        {
            var textString = Services.Get<Translator>().GetTranslation(textId);
            if (!string.IsNullOrWhiteSpace(textString))
                text.text = textString;
        }
    
        private void OnDestroy()
        {
            Events.LanguageChanged -= OnLanguageChanged;
        }
    }
}
