
using System.Threading.Tasks;

namespace General.Scripts
{
    public interface ICloseController
    {
        public Task Close();
        public void Exit();
    }
}
