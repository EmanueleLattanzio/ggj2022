using System;
using GameManager.Scripts;
using Settings.Scripts;
using Unity.Notifications.iOS;
using UnityEngine;
using UnityEngine.UI;

namespace General.Scripts
{
    public class ButtonOnOffSettings : MonoBehaviour
    {
        [SerializeField] private SettingsPreferences.Settings setting;
        [SerializeField] private Animator animator;
        [SerializeField] private Button button;
        
        private static readonly int ButtonState = Animator.StringToHash("OnState");
        private static readonly int ButtonInit = Animator.StringToHash("Init");
        private bool _buttonActive;
        private SettingsContext _settingsContext;
        
        private void OnEnable()
        {
            if (_settingsContext == null)
                _settingsContext = (SettingsContext) Services.Get<ContextManager>().Get(SceneName.Settings);
            var isActive = _settingsContext.Settings.IsActive(setting);
            
            //TODO solo su IOS
            if (setting == SettingsPreferences.Settings.Notifications && Application.platform == RuntimePlatform.IPhonePlayer &&
                iOSNotificationCenter.GetNotificationSettings().NotificationCenterSetting != NotificationSetting.Enabled)
            {
                isActive = false;
                button.enabled = false;
            }
            _buttonActive = isActive;
            animator.SetBool(ButtonInit, true);
            animator.SetBool(ButtonState, isActive);
        }

        public async void ChangeStatus()
        {
            var isActive = _settingsContext.Settings.IsActive(setting);
            _buttonActive = !isActive;
            
            var task = setting switch
            {
                SettingsPreferences.Settings.Music => Services.Get<Mixer>().SetMusicActive(_buttonActive),
                SettingsPreferences.Settings.SoundEffects => Services.Get<Mixer>().SetSoundsEffectsActive(_buttonActive),
                SettingsPreferences.Settings.Vibrations => Services.Get<Vibrator>().SetVibrationActive(_buttonActive),
                SettingsPreferences.Settings.Notifications => Services.Get<Notificator>().SetNotificationsActive(_buttonActive),
                _ => throw new ArgumentOutOfRangeException(nameof(setting), setting, null)
            };
            
            animator.SetBool(ButtonState, _buttonActive);
            if (!await task)
            {
                //TODO ToastService error
                animator.SetBool(ButtonState, isActive);
            }
        }
    }
}
