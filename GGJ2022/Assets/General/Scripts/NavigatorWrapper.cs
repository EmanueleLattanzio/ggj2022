using System;
using GameManager.Scripts;
using UnityEngine;

namespace General.Scripts
{
    [Serializable]
    public class NavigatorWrapper : ScriptableObject
    {
        public void Open(string sceneName)
        {
            if (Enum.TryParse<SceneName>(sceneName, out var res))
                Services.Get<Navigator>().Open(res);
            
        }

        public void Back()
        {
            Services.Get<Navigator>().Back();
        }

        public void Back(int pages)
        {
            Services.Get<Navigator>().Back(pages);
        }

        public void CloseApp()
        {
#if UNITY_EDITOR
            UnityEditor.EditorApplication.isPlaying = false;
#else
            Application.Quit();
#endif
        }
    }
}