using System;
using GameManager.Scripts;
using Settings.Scripts;
using UnityEngine;

namespace General.Scripts
{
    [Serializable]
    public class MixerWrapperButtonActivator : MonoBehaviour
    {
        [SerializeField] private GameObject soundActivateButton;
        [SerializeField] private GameObject soundDeactivateButton;
        public void OnEnable()
        {
            var isActive = ((SettingsContext) Services.Get<ContextManager>().Get(SceneName.Settings)).Settings.Music;
            soundActivateButton.SetActive(isActive);
            soundDeactivateButton.SetActive(!isActive);
        }
    }
}